package com.christofer.android.gumtreetest;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.android.gms.maps.MapView;

/**
 * Fragment containing the views describing the product details.
 */
public class ProductDetailsFragment extends Fragment {

    private TextView addressView;
    private MapView mapView;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.view__product_profile_details, container, false);
        setupAddressView(rootView);
        setupMapView(savedInstanceState, rootView);
        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        mapView.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        mapView.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mapView != null)
            mapView.onDestroy();
    }

    private void setupAddressView(View rootView) {
        addressView = (TextView) rootView.findViewById(R.id.view_product_profile_details_address);
        addressView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_VIEW,
                        Uri.parse("geo:" + 0 + "," + 0 + "?q=" + ((TextView) v).getText()));
                startActivity(intent);
            }
        });
    }

    private void setupMapView(Bundle savedInstanceState, View rootView) {
        mapView = (MapView) rootView.findViewById(R.id.content_box_map_map);
        mapView.onCreate(savedInstanceState);
    }
}
