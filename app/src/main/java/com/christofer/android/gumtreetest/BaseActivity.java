package com.christofer.android.gumtreetest;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;

/**
 * Base activity containing a toolbar.
 */
public abstract class BaseActivity extends ActionBarActivity {

    private Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayoutResource());
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        if (toolbar != null) {
            setSupportActionBar(toolbar);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
    }

    /**
     * Starts a fragment to be shown to the users.
     *
     * @param fragment
     * @param tag
     * @param resourceId
     * @param addToBackStack
     */
    public void startFragment(Fragment fragment, String tag, int resourceId, boolean addToBackStack) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction().add(resourceId, fragment, tag);
        if(addToBackStack){
            fragmentTransaction = fragmentTransaction.addToBackStack(tag);
        }
        fragmentTransaction.commit();
    }

    /**
     * Starts a fragment to be shown to the user by replacing an old one.
     *
     * @param fragment
     * @param tag
     * @param resourceId
     */
    public void replaceFragment(Fragment fragment, String tag, int resourceId, boolean addToBackStack) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction().replace(resourceId, fragment, tag);
        if(addToBackStack){
            fragmentTransaction = fragmentTransaction.addToBackStack(tag);
        }

        fragmentTransaction.commit();
    }

    /**
     * Get the layout resource id that should be inflated.
     *
     * @return
     */
    protected abstract int getLayoutResource();

}