package com.christofer.android.gumtreetest;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.view.MenuItem;
import android.support.v4.view.ViewPager;
import android.view.Menu;
import android.widget.TextView;

import java.util.List;
import java.util.Locale;


public class MainActivity extends BaseActivity {

    private static final String FRAGMENT_PRODUCT_DETAILS = "fragment_product_details";
    private SectionsPagerAdapter sectionsPagerAdapter;
    private ViewPager viewPager;
    private TextView numOfImagesView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setupViewPager();
        setupNumOfImages();
        startProductDetailsFragment();
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_main;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        MenuItem item = menu.findItem(R.id.share);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.share) {
            shareItem();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void shareItem() {
        Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
        startActivity(Intent.createChooser(sharingIntent, getString(R.string.share_msg)));
    }

    private void setupViewPager() {
        sectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager(), MockData.getProductImages());
        viewPager = (ViewPager) findViewById(R.id.activity_main_fragment_product_images);
        viewPager.setAdapter(sectionsPagerAdapter);
    }

    private void startProductDetailsFragment() {
        ProductDetailsFragment fragment = new ProductDetailsFragment();
        startFragment(fragment, FRAGMENT_PRODUCT_DETAILS, R.id.activity_main_fragment_product_details, false);
    }

    private void setupNumOfImages() {
        numOfImagesView = (TextView) findViewById(R.id.activity_main_fragment_product_num_of_images);
        numOfImagesView.setText(String.valueOf(MockData.getProductImages().size()));
    }

    /**
     * A {@link android.support.v4.app.FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        private List<String> images;

        public SectionsPagerAdapter(FragmentManager fm, List<String> images) {
            super(fm);
            this.images = images;
        }

        @Override
        public Fragment getItem(int position) {
            return ProductImageFragment.newInstance(images.get(position));
        }

        @Override
        public int getCount() {
            return images.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            Locale l = Locale.getDefault();
            return getString(R.string.title_section1).toUpperCase(l);

        }
    }
}