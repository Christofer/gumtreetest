package com.christofer.android.gumtreetest;

import java.util.ArrayList;
import java.util.List;

/**
 * Mock data for demonstration of the UI.
 */
public class MockData {

    public static final String[] MOCK_IMAGE_URLS = new String[]{"http://fc03.deviantart.net/fs70/i/2011/141/8/b/1991_ferrari_348_ts_1_by_roddy1990-d3gvdee.jpg", "http://i.ebayimg.com/00/s/NTk3WDgwMA==/z/DRsAAOSwofxUgtQB/$_86.JPG", "https://c1.staticflickr.com/9/8453/8027490601_ce632ff628_z.jpg", "http://i.kinja-img.com/gawker-media/image/upload/s--tsrdAWm6--/18ay2f0aqppw1jpg.jpg", "http://www.autodrome-cannes.com/ferrari_348_tb_interior.jpg"};
    private static final String LATITUDE = "51.5072";
    private static final String LONGITUDE = "0.1275";

    public static List<String> getProductImages() {
        List<String> images = new ArrayList<>();
        for (int i = 0; i < MOCK_IMAGE_URLS.length; i++) {
            images.add(MOCK_IMAGE_URLS[i]);
        }
        return images;
    }

    public static String getLatitude() {
        return LATITUDE;
    }

    public static String getLongitude() {
        return LONGITUDE;
    }
}
