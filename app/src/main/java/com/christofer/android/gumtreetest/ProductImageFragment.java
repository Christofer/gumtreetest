package com.christofer.android.gumtreetest;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

/**
 * Fragment containing an image of the profile of a product.
 */
public class ProductImageFragment extends Fragment {

    private static final String IMAGE_URL = "image_url";
    private ImageView imageView;
    private String imageUrl;

    public static ProductImageFragment newInstance(String imageUrl) {
        Bundle extras = new Bundle();
        extras.putString(IMAGE_URL, imageUrl);
        ProductImageFragment fragment = new ProductImageFragment();
        fragment.setArguments(extras);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        imageUrl = getArguments().getString(IMAGE_URL);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.item_product_picture, container, false);
        imageView = (ImageView) rootView.findViewById(R.id.item_product_picture_imageview);
        populateImage();
        return rootView;
    }

    private void populateImage() {
        Context context = imageView.getContext();
        if (context == null) {
            return;
        }
        Picasso.with(context).cancelRequest(imageView);
        int height = (int) context.getResources().getDimension(R.dimen.view_product_profile_image_height);
        int width = (int) context.getResources().getDimension(R.dimen.view_product_profile_image_width);

        Picasso.with(context).load(imageUrl).resize(width, height).centerCrop().into(imageView);
    }

}